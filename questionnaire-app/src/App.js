import './App.css';
import { Routes, Route } from "react-router-dom";
import QuestionFormRouter from './components/QuestionForm/QuestionFormRouter';
import QuestionFormN from './components/QuestionForm/QuestionFormN';
import QuestionFormM from './components/QuestionForm/QuestionFormM';
import QuestionFormB from './components/QuestionForm/QuestionFormB';
import LoginForm from './components/LoginForm';


function App() {
  return (
    <div> 
      <QuestionFormRouter></QuestionFormRouter>
    </div>
     
  );
}

export default App;
