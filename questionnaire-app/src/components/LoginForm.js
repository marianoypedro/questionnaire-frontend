import React, { useState } from "react"
import axios from "axios"

function LoginForm(){

    const[email, setEmail] = useState([]);
    const baseURL = "http://localhost:8080/api/User/addUser";

    function onSubmitHandler(){ 
        localStorage.setItem('email', JSON.stringify(email));
        let  emailC = {
            email: email
        };
        axios.post(baseURL, emailC
        ).then((response) => {
            setEmail(response.data);
        });
    }

    

    return(<div>
        <form onSubmit={onSubmitHandler}>
            <label>Email</label>
            <input type='text' id = "email-field" onChange={event => setEmail(event.target.value)}/>
            <button>Submit</button>
        </form>
    </div>);
}

export default LoginForm;