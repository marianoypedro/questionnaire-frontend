import React, { useState } from "react";
import { useActionData } from "react-router-dom";

function QuestionFormN(props, {answerHandler}){

    const [message, setMessage] = useState('');

    const newFunction = (e) => {
        setMessage(e.target.value);
        props.answerHandler(e, message);
    }    

return(<div>
    <legend ><center><p></p></center></legend><br/>
        <div>
                <form className="well form-horizontal " action=" " method="post"  id="multiple_question_fomr">
                    <fieldset>
                    <div className="form-group">
                    <label className="col-md-4 control-label">{props.question}</label>
                    <div className="col-md-4 inputGroupContainer">
                    <div className="input-group">
                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                    <label className="col-md-4 control-label">Put Your Answer Here</label>
                    <input name="answer1" className="form-control" value={message} type="number" min={0} max = {10} onChange={newFunction} />
                    </div>
                    </div>
                    </div>
                    </fieldset>
                </form>
            </div>
</div>);

}

export default QuestionFormN;