import React,{useState} from "react";


function QuestionFormB(props, {answerHandler}){
    const [message, setMessage] = useState('');

    const newFunction = (e) => {
        setMessage(e.target.value);
        props.answerHandler(e, message);
    }   

    return(<div>
        <legend ><center><p></p></center></legend><br/>
            <div>
                    <form className="well form-horizontal " action=" " method="post"  id="multiple_question_fomr">
                        <fieldset>
                        <div className="form-group">
                        <label className="col-md-4 control-label">{props.question}</label>
                        <div className="col-md-4 inputGroupContainer">
                        <div className="input-group">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                        <label className="col-md-4 control-label">True</label>
                        <input name="answer1" className="form-control" value={props.answer} type = "radio" onChange={newFunction}/>
                        <label className="col-md-4 control-label">False</label>
                        <input name="answer1" className="form-control" value={props.answer} type = "radio" onChange={newFunction}/>
                        </div>
                        </div>
                        </div>
                        </fieldset>
                    </form>
                </div>
    </div>);

}

export default QuestionFormB;