import React, { useEffect,useState } from "react"
import axios from "axios"
import LoginForm from "../LoginForm";
import QuestionFormB from "./QuestionFormB";
import QuestionFormN from "./QuestionFormN";
import QuestionFormM from "./QuestionFormM";


function QuestionFormRouter(){

    const[questions, setQuestions] = useState([]);
    const[questionIndex, setQuestionIndex] = useState(0);
    const[answer, setAnswer] = useState([]);

    const currentQuestion = questions[questionIndex];
    const baseURLget= "http://localhost:8080/api/Question/getQuestion";
    const baseURLpost="http://localhost:8080/api/Answer/addAnswer"

    useEffect(() => {
        axios.get(baseURLget).then((responseQuestions) =>{
            setQuestions(responseQuestions.data);
        }).catch(error => console.error(`Error: ${error}`));
    }, []);

    async function onClickHandler(event){
        event.preventDefault(); 

        console.log("Index"+questionIndex);
        setQuestionIndex((currentQuestionIndex) => currentQuestionIndex + 1);
    
        axios.post(baseURLpost, answer
            ).then((responseAnswers) => {
                setAnswer(responseAnswers.data);
            }).catch(error => console.error(`Error: ${error}`));
    }

    function answerHandler(event, msg){
        console.log(msg);
        const  answerC = {
            answer: event.target.value,
            questionid: currentQuestion.id,
            email: localStorage.getItem("email")
        };
        setAnswer(answerC);
    }

    return(
        <div>
            <h1>{questionIndex}</h1>
            <LoginForm></LoginForm>
            { currentQuestion ?
                {
                    1: <QuestionFormB question = {currentQuestion.question} answerHandler = {answerHandler}></QuestionFormB>,
                    2: <QuestionFormN question = {currentQuestion.question} answerHandler = {answerHandler}></QuestionFormN>,
                    3: <QuestionFormM question = {currentQuestion.question} 
                    answer1={currentQuestion.multiple1} 
                    answer2={currentQuestion.multiple2} 
                    answer3={currentQuestion.multiple3} 
                    answer4={currentQuestion.multiple4} 
                    answer5={currentQuestion.multiple5}
                    answerHandler = {answerHandler}></QuestionFormM>
                }[currentQuestion.type] :
                <p>Loading...</p>
            }
            <button type="submit" className="btn btn-success" onClick={onClickHandler}>Next Form<span className="glyphicon glyphicon-send"></span></button>
        </div>
    );
}

export default QuestionFormRouter;