import React, { useState, useEffect } from "react"
import axios from "axios"


function QuestionFormM(props, {answerHandler}){


    return (
        <div>
            <legend ><center><p></p></center></legend><br/>
                <div>
                        <form className="well form-horizontal " action=" " method="post"  id="multiple_question_fomr">
                            <fieldset>
                            <div className="form-group">
                            <label className="col-md-4 control-label">{props.question}</label>
                            <div className="col-md-4 inputGroupContainer">
                            <div className="input-group">
                            <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                            <label className="col-md-4 control-label">{props.answer1}</label>
                            <input name="answer1" className="form-control" value={props.answer1} type="radio"/>
                            <label className="col-md-4 control-label">{props.answer2}</label>
                            <input name="answer2" className="form-control" value={props.answer2} type="radio"/>
                            <label className="col-md-4 control-label">{props.answer3}</label>
                            <input name="answer3" className="form-control" value={props.answer3} type="radio"/>
                            <label className="col-md-4 control-label">{props.answer4}</label>
                            <input name="answer4" className="form-control" value={props.answer4} type="radio"/>
                            <label className="col-md-4 control-label">{props.answer5}</label>
                            <input name="answer5" className="form-control" value={props.answer5} type="radio"/>
                            </div>
                            </div>
                            </div>
                            </fieldset>
                        </form>
                    </div>
        </div>);
}

export default QuestionFormM;